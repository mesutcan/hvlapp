package com.mobile.hvlapp.enumeration;

import com.mobile.hvlapp.R;

public enum EventType {
    BIRTH_DATE(R.drawable.birthday_icon),        //DOGUM TARIHI
    TRAINING(R.drawable.blue_circle),           //EGITIM
    ANNUAL_PAID_LEAVE(R.drawable.red_circle);   //YILLIK UCRETLI IZIN

    private int icon;

    EventType(int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }
}
