package com.mobile.hvlapp.adapter;

import android.content.Context;
import android.graphics.drawable.Icon;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobile.hvlapp.R;
import com.mobile.hvlapp.service.dto.Event;

import java.util.ArrayList;
import java.util.Calendar;

public class CustomCalendarDetailListAdapter extends ArrayAdapter<Event> {

    private final LayoutInflater inflater;
    private final Context context;
    private ViewHolder holder;
    private final ArrayList<Event> eventList;

    public CustomCalendarDetailListAdapter(Context context, ArrayList<Event> eventList) {
        super(context, 0, eventList);
        this.context = context;
        this.eventList = eventList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return eventList.size();
    }

    @Override
    public Event getItem(int position) {
        return eventList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.calendar_detail_row, null);
            holder = new ViewHolder();
            holder.header = convertView.findViewById(R.id.header);
            holder.time = convertView.findViewById(R.id.time);
            holder.eventTypeIcon = convertView.findViewById(R.id.eventIcon);


            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Event event = eventList.get(position);
        if (event != null) {
            holder.header.setText(event.getHeader());
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(event.getTime());
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            String strMinute;
            if (minute < 10) {
                strMinute = "0" + minute;
            } else {
                strMinute = String.valueOf(minute);
            }
            holder.time.setText(hour + ":" + strMinute);
            holder.eventTypeIcon.setImageIcon(Icon.createWithResource(getContext(),event.getEventType().getIcon()));
        }
        return convertView;
    }

    private static class ViewHolder {
        TextView header;
        TextView time;
        ImageView eventTypeIcon;
    }
}
