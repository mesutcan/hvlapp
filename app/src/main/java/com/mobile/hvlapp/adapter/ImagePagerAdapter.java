package com.mobile.hvlapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.mobile.hvlapp.R;
import com.mobile.hvlapp.service.dto.News;

import java.util.ArrayList;
import java.util.List;

public class ImagePagerAdapter extends PagerAdapter {

    private Context context;
    private List<News> newsList = new ArrayList<>();

    public ImagePagerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return !newsList.isEmpty() ? newsList.size() : 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return object == view;
    }

    public List<News> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<News> newsList) {
        this.newsList = newsList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_image, null);
        ImageView imageView = view.findViewById(R.id.image);
        String url = !newsList.isEmpty() ? newsList.get(position).getUrl() : null;
        Glide.with(context)
                .load(url)
                .placeholder(context.getResources().getDrawable(R.color.cardview_dark_background))
                .into(imageView);

        View cardView = LayoutInflater.from(context).inflate(R.layout.layout_image_slider, null);
        String waterMarkStr = !newsList.isEmpty() ? newsList.get(position).getText() : null;
        TextView waterMarkText = cardView.findViewById(R.id.water_mark);
        waterMarkText.setText(waterMarkStr);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object view) {
        container.removeView((View) view);
    }
}