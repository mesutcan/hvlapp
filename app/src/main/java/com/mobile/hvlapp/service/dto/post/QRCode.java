package com.mobile.hvlapp.service.dto.post;

import java.io.Serializable;

public class QRCode implements Serializable {

    private String pinCode;

    private String id;

    private int width;

    private int height;

    public QRCode(String pinCode, String id, int width, int height) {
        this.pinCode = pinCode;
        this.id = id;
        this.width = width;
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}