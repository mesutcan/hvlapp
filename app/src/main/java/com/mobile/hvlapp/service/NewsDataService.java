package com.mobile.hvlapp.service;

import com.mobile.hvlapp.service.dto.News;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface NewsDataService {

    @GET("/news/getAll")
    Call<List<News>> getNews();
}