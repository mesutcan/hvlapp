package com.mobile.hvlapp.service;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CarParkDataService {

    @GET("/app/emptySeat")
    Call<Integer> getEmptyParkCount();

}
