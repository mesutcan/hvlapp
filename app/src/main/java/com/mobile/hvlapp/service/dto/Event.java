package com.mobile.hvlapp.service.dto;

import com.applandeo.materialcalendarview.EventDay;
import com.google.gson.annotations.SerializedName;
import com.mobile.hvlapp.enumeration.EventType;

public class Event {

    @SerializedName("id")
    private int id;

    @SerializedName("eventType")
    private EventType eventType;

    @SerializedName("time")
    private long time;

    @SerializedName("header")
    private String header;

    private EventDay eventDay;


    public Event(int id, EventType eventType, long time, String header, EventDay eventDay) {
        this.id = id;
        this.eventType = eventType;
        this.time = time;
        this.header = header;
        this.eventDay = eventDay;
    }

    public Event() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public EventDay getEventDay() {
        return eventDay;
    }

    public void setEventDay(EventDay eventDay) {
        this.eventDay = eventDay;
    }
}
