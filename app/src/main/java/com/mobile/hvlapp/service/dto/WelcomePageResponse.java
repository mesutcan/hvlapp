package com.mobile.hvlapp.service.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WelcomePageResponse implements Serializable {

    @SerializedName("employee")
    private Employee employee;

    @SerializedName("message")
    private String message;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}