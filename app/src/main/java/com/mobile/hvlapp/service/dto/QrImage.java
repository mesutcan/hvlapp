package com.mobile.hvlapp.service.dto;

import com.google.gson.annotations.SerializedName;

public class QrImage {
    @SerializedName("base64String")
    private String base64String;

    public QrImage(String base64String) {
        this.base64String = base64String;
    }

    public String getBase64String() {
        return base64String;
    }

    public void setBase64String(String base64String) {
        this.base64String = base64String;
    }
}
