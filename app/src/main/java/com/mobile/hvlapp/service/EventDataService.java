package com.mobile.hvlapp.service;

import com.mobile.hvlapp.service.dto.Event;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EventDataService {

    @GET("/event/getAll")
    Call<List<Event>> getAllEvents();

}
