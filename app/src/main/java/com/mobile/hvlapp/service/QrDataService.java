package com.mobile.hvlapp.service;

import com.mobile.hvlapp.service.dto.QrImage;
import com.mobile.hvlapp.service.dto.post.QRCode;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface QrDataService {

    @POST("/qrcode/generateQRCodeImage")
    Call<QrImage> generateQrCodeImage(@Body QRCode qrCode);
}