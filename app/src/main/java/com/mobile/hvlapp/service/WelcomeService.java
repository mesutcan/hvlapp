package com.mobile.hvlapp.service;

import com.mobile.hvlapp.service.dto.WelcomePageResponse;
import com.mobile.hvlapp.service.dto.post.WelcomePageRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface WelcomeService {

    @POST("/employee/welcomePage")
    Call<WelcomePageResponse> prepareWelcomePage(@Body WelcomePageRequest request);
}