package com.mobile.hvlapp.service.dto.post;

import java.io.Serializable;

public class WelcomePageRequest implements Serializable {

    private String pinCode;
    private int id;

    public WelcomePageRequest(String pinCode, int id) {
        this.pinCode = pinCode;
        this.id = id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }
}