package com.mobile.hvlapp.service.dto;

import com.google.gson.annotations.SerializedName;

public class News {
    @SerializedName("url")
    private String url;
    @SerializedName("text")
    private String text;

    public News(String url, String text) {
        this.url = url;
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

