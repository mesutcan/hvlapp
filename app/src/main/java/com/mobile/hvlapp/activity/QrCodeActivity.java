package com.mobile.hvlapp.activity;

import android.os.Bundle;
import android.util.Base64;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.mobile.hvlapp.R;

public class QrCodeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);
        String bitmapStr = getIntent().getStringExtra("QR_IMAGE");

        ImageView imageView = findViewById(R.id.qr_code_image);
        Glide.with(this).load(Base64.decode(bitmapStr, Base64.DEFAULT)).fitCenter().into(imageView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
