package com.mobile.hvlapp.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.mobile.hvlapp.R;
import com.mobile.hvlapp.adapter.CustomCalendarDetailListAdapter;
import com.mobile.hvlapp.service.EventDataService;
import com.mobile.hvlapp.service.RetrofitClient;
import com.mobile.hvlapp.service.dto.Event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CalendarActivity extends AppCompatActivity {

    private List<EventDay> eventDays = new ArrayList<>(0);
    private List<Event> eventList = new ArrayList<>(0);
    private ProgressDialog progressDoalog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity);
        progressDoalog = new ProgressDialog(CalendarActivity.this);
        progressDoalog.setMessage("Etkinlik listesi getiriliyor....");
        progressDoalog.show();

        EventDataService eventDataService = RetrofitClient.getRetrofitInstance().create(EventDataService.class);
        Call<List<Event>> allEvents = eventDataService.getAllEvents();
        allEvents.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                progressDoalog.dismiss();
                generateEventDayList(response.body());
                updateCalender();
                Log.e(this.getClass().getName(), "onResponse: " + response.body());
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(getApplicationContext(), "Takvim verileri alınırken bir hata oluştu!", Toast.LENGTH_LONG).show();
                Log.e(this.getClass().getName(), "onFailure: Error: ");
            }
        });
        updateCalender();
        CalendarView calendarView = findViewById(R.id.calendarView);
        calendarView.setEvents(eventDays);

        calendarView.setOnDayClickListener(eventDay -> {
            int dayOfEvent = eventDay.getCalendar().get(Calendar.DAY_OF_MONTH);
            int yearOfEvent = eventDay.getCalendar().get(Calendar.YEAR);
            int monthOfEvent = eventDay.getCalendar().get(Calendar.MONTH) + 1;

            ArrayList<Event> selectedDayEventList = new ArrayList<>();
            for (Event tempEvent : eventList) {
                int yearOfTempEvent = tempEvent.getEventDay().getCalendar().get(Calendar.YEAR);
                int dayOfTempEvent = tempEvent.getEventDay().getCalendar().get(Calendar.DAY_OF_MONTH);
                int monthOfTempEvent = tempEvent.getEventDay().getCalendar().get(Calendar.MONTH) + 1;

                if (dayOfEvent == dayOfTempEvent && monthOfEvent == monthOfTempEvent && yearOfEvent == yearOfTempEvent) {
                    selectedDayEventList.add(tempEvent);
                }
            }
            ListView viewById = findViewById(R.id.calendar_detail_list);
            CustomCalendarDetailListAdapter adapter = new CustomCalendarDetailListAdapter(CalendarActivity.this, selectedDayEventList);
            viewById.setAdapter(adapter);

            Toast.makeText(this, dayOfEvent + "/" + monthOfEvent + "/" + yearOfEvent, Toast.LENGTH_LONG).
                    show();
        });
    }

    private void updateCalender() {
        CalendarView calendarView = findViewById(R.id.calendarView);
        calendarView.setEvents(eventDays);
    }

    private void generateEventDayList(List<Event> eventList) {
        this.eventList = eventList;
        for (Event event : this.eventList) {
            EventDay eventDay = new EventDay(getCalendar(event.getTime()), event.getEventType().getIcon());
            eventDays.add(eventDay);
            event.setEventDay(eventDay);
        }
    }

    private Calendar getCalendar(long time) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(time);
        return instance;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

}
