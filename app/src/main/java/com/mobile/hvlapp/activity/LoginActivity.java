package com.mobile.hvlapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.mobile.hvlapp.MainActivity;
import com.mobile.hvlapp.R;
import com.mobile.hvlapp.service.RetrofitClient;
import com.mobile.hvlapp.service.WelcomeService;
import com.mobile.hvlapp.service.dto.WelcomePageResponse;
import com.mobile.hvlapp.service.dto.post.WelcomePageRequest;
import com.mobile.hvlapp.utils.ApplicationUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPref = this.getSharedPreferences(ApplicationUtil.PREFS_FILE_NAME, Context.MODE_PRIVATE);
        if (sharedPref.getInt(ApplicationUtil.PREFS_EMPLOYEE_ID_KEY, 0) != 0)
            startMainActivity();
    }

    public void prepareWelcome(View view) {
        EditText regNoTxt = view.getRootView().findViewById(R.id.reg_no);
        EditText pin = view.getRootView().findViewById(R.id.pin);

        WelcomeService welcomeService = RetrofitClient.getRetrofitInstance().create(WelcomeService.class);
        welcomeService.prepareWelcomePage(new WelcomePageRequest(pin.getText().toString(), Integer.parseInt(regNoTxt.getText().toString())))
                .enqueue(new Callback<WelcomePageResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<WelcomePageResponse> call, @NonNull Response<WelcomePageResponse> response) {

                        SharedPreferences.Editor edit = sharedPref.edit();
                        WelcomePageResponse welcomePageResponse = response.body();
                        edit.putInt(ApplicationUtil.PREFS_EMPLOYEE_ID_KEY, welcomePageResponse.getEmployee().getEmpId());
                        edit.putString(ApplicationUtil.PREFS_EMPLOYEE_PIN_KEY, welcomePageResponse.getEmployee().getPinCode());
                        edit.putString(ApplicationUtil.PREFS_WELCOME_MESSAGE, welcomePageResponse.getMessage());
                        edit.apply();

                        startMainActivity();
                    }

                    @Override
                    public void onFailure(@NonNull Call<WelcomePageResponse> call, @NonNull Throwable t) {

                    }
                });
    }

    private void startMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
