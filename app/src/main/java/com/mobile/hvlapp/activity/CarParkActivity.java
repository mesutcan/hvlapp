package com.mobile.hvlapp.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.hvlapp.R;
import com.mobile.hvlapp.service.CarParkDataService;
import com.mobile.hvlapp.service.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarParkActivity extends Activity {

    private ProgressDialog progressDoalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_park);



        progressDoalog = new ProgressDialog(CarParkActivity.this);
        progressDoalog.setMessage("Otopark listesi getiriliyor....");
        progressDoalog.show();

        CarParkDataService carParkDataService = RetrofitClient.getRetrofitInstance().create(CarParkDataService.class);
        Call<Integer> emptyParkCount = carParkDataService.getEmptyParkCount();
        emptyParkCount.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                progressDoalog.dismiss();
                Integer body = response.body();
                TextView viewById = (TextView) findViewById(R.id.carpark_avaliable_lot_count);
                viewById.setText(body.toString());
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(getApplicationContext(), "Otopark verileri alınırken bir hata oluştu!", Toast.LENGTH_LONG).show();
                Log.e(this.getClass().getName(), "onFailure: Error: ");
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}
