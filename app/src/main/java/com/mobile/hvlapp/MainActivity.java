package com.mobile.hvlapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.mobile.hvlapp.activity.CalendarActivity;
import com.mobile.hvlapp.activity.CarParkActivity;
import com.mobile.hvlapp.activity.DiningHallActivity;
import com.mobile.hvlapp.activity.QrCodeActivity;
import com.mobile.hvlapp.adapter.ImagePagerAdapter;
import com.mobile.hvlapp.service.CarParkDataService;
import com.mobile.hvlapp.service.NewsDataService;
import com.mobile.hvlapp.service.QrDataService;
import com.mobile.hvlapp.service.RetrofitClient;
import com.mobile.hvlapp.service.dto.News;
import com.mobile.hvlapp.service.dto.QrImage;
import com.mobile.hvlapp.service.dto.post.QRCode;
import com.mobile.hvlapp.utils.AppUtils;
import com.mobile.hvlapp.utils.ApplicationUtil;

import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private ImagePagerAdapter imagePagerAdapter;
    private CircleIndicator circleIndicator;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = this.getSharedPreferences(ApplicationUtil.PREFS_FILE_NAME, Context.MODE_PRIVATE);
        configureNewsSlider();
        setWelcomeText();
        getParkCount();
    }

    private void setWelcomeText() {
        TextView welcomeText = findViewById(R.id.welcomeText);
        welcomeText.setText(sharedPreferences.getString(ApplicationUtil.PREFS_WELCOME_MESSAGE, "")+ ", 534 Yıldız");
    }

    public void startCalendarActivity(View view) {
        Intent intent = new Intent(this, CalendarActivity.class);
        startActivity(intent);
    }

    public void startDiningHallActivity(View view) {
        Intent intent = new Intent(this, DiningHallActivity.class);
        startActivity(intent);
    }

    public void startCarParkActivity(View view) {
        Intent intent = new Intent(this, CarParkActivity.class);
        startActivity(intent);
    }

    private void configureNewsSlider() {
        getNews();
        viewPager = findViewById(R.id.image_pager);
        imagePagerAdapter = new ImagePagerAdapter(this);
        viewPager.setAdapter(imagePagerAdapter);
        AppUtils.automaticSlide(viewPager, imagePagerAdapter);
        circleIndicator = findViewById(R.id.circle);
        circleIndicator.setViewPager(viewPager);
    }

    private void getParkCount(){
        CarParkDataService carParkDataService = RetrofitClient.getRetrofitInstance().create(CarParkDataService.class);
        Call<Integer> emptyParkCount = carParkDataService.getEmptyParkCount();
        emptyParkCount.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                Integer body = response.body();
                TextView carParkText = findViewById(R.id.carparktext);
                carParkText.setText("Boş Park Yeri: "+body);
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.e(this.getClass().getName(), "onFailure: Error: ");
            }
        });
    }

    private void getNews() {
        NewsDataService newsDataService = RetrofitClient.getRetrofitInstance().create(NewsDataService.class);
        newsDataService.getNews().enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(@NonNull Call<List<News>> call, @NonNull Response<List<News>> response) {
                imagePagerAdapter.setNewsList(response.body());
                imagePagerAdapter.notifyDataSetChanged();
                circleIndicator = findViewById(R.id.circle);
                circleIndicator.setViewPager(viewPager);
            }

            @Override
            public void onFailure(@NonNull Call<List<News>> call, @NonNull Throwable t) {

            }
        });
    }

    public void generateQr(View view) {
        int height = Resources.getSystem().getDisplayMetrics().heightPixels;
        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        String regNo = String.valueOf(sharedPreferences.getInt(ApplicationUtil.PREFS_EMPLOYEE_ID_KEY, 0));
        String pin = sharedPreferences.getString(ApplicationUtil.PREFS_EMPLOYEE_PIN_KEY, "");
        QRCode qrCode = new QRCode(pin, regNo, width, height);
        QrDataService qrDataService = RetrofitClient.getRetrofitInstance().create(QrDataService.class);
        qrDataService.generateQrCodeImage(qrCode).enqueue(new Callback<QrImage>() {
            @Override
            public void onResponse(@NonNull Call<QrImage> call, @NonNull Response<QrImage> response) {
                Intent intent = new Intent(getApplicationContext(), QrCodeActivity.class);
                intent.putExtra("QR_IMAGE", response.body().getBase64String());
                startActivity(intent);
            }

            @Override
            public void onFailure(@NonNull Call<QrImage> call, @NonNull Throwable t) {
                Log.e(TAG, "Error: ");
            }
        });

    }


}
