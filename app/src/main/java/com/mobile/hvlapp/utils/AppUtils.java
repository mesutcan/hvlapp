package com.mobile.hvlapp.utils;

import android.os.Handler;

import androidx.viewpager.widget.ViewPager;

import com.mobile.hvlapp.adapter.ImagePagerAdapter;

public final class AppUtils {

    public static void automaticSlide(ViewPager viewPager, ImagePagerAdapter imagePagerAdapter) {
        final int DELAY = 5000;
        final Handler mHandler = new Handler();
        Runnable runnable = new Runnable() {
            int page = 0;
            boolean first = true;

            public void run() {
                if (!first) {
                    page = viewPager.getCurrentItem();
                    if ((imagePagerAdapter.getCount() - 1) == page) {
                        page = 0;
                    } else {
                        page++;
                    }
                    viewPager.setCurrentItem(page, true);
                } else {
                    first = false;
                }
                mHandler.postDelayed(this, DELAY);
            }
        };
        runnable.run();
    }

    /*public static Bitmap convertBitMatrixToBitMap(BitMatrix bitMatrix, Context context) {
        int bitMatrixWidth = bitMatrix.getWidth();
        int bitMatrixHeight = bitMatrix.getHeight();
        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;
            for (int x = 0; x < bitMatrixWidth; x++) {
                pixels[offset + x] = bitMatrix.get(x, y) ? ContextCompat.getColor(context, R.color.black) : ContextCompat.getColor(context, R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);
        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }*/
}
