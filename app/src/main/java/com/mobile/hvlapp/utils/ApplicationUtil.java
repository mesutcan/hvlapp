package com.mobile.hvlapp.utils;

public class ApplicationUtil {

    private ApplicationUtil() {
    }

    public static final String PREFS_FILE_NAME = "HVL_PREFS";
    public static final String PREFS_EMPLOYEE_PIN_KEY = "emp_pin";
    public static final String PREFS_EMPLOYEE_ID_KEY = "emp_id";
    public static final String PREFS_WELCOME_MESSAGE = "welcome_message";
}
